package testJmsGenerico;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageProducer;
import javax.jms.Session;

import com.ibm.websphere.sib.api.jms.JmsConnectionFactory;
import com.ibm.websphere.sib.api.jms.JmsFactoryFactory;
import com.ibm.websphere.sib.api.jms.JmsQueue;



public class TestJMS {

	private void testScriviCoda(String[] args) throws Exception {

		Session session = null;
		MessageProducer producer = null;

		try {
//			local walter
//			String nomeCoda = "miurANS_Queue";
//			String serverBus = "server2_bus";
//			String endPoint = "localhost:7277";
//			local bera
			// ******Colladuo******************
//			String nomeCoda = "inQueue"; //"miurANS_Queue";
//			String serverBus = "server1_bus";
//			String endPoint = "10.34.222.211:7278";
			// ********************************
//			String serverBus = "server2_bus";
//			String endPoint = "10.34.222.213:7279";
//			websphere 85
//			String nomeCoda = "inQueue"; //"miurANS_Queue";
//			String serverBus = "server2_bus";
//			String endPoint = "localhost:7276";
//			String nomeCoda = "cvQueue"; //"miurANS_Queue";
//			String serverBus = "server2_bus";
//			String endPoint = "192.168.200.98:7279";
			
//			String nomeCoda = "inQueue"; //"miurANS_Queue";
//			String serverBus = "server2_bus";
//			String endPoint = "localhost:7276";
			// Cirella
//			String nomeCoda = "testQueue"; //"miurANS_Queue";
//			String serverBus = "server_bus1";
//			String endPoint = "localhost:7278";

			
			// modifica per parametri provenienti dalla chiamata tramite riga di comando
			/*
			 * 3 argomenti:  nome coda, server Bus ed endpoint
			 * 
			 * ARCUMENTS : (*) REQUIRE 
			 * [0] NOME_CODA (*)
			 * [1] SERVER_BUS (*)
			 * [2] END_POINT (*)
			 */
			
			 if (args==null || args.length<=0) {
				 System.out.println("Nessun parametro passato!");
				 System.exit(1);
			 }
			 if(args.length == 3){
				 
				 String nomeCoda = args[0];
				 String serverBus = args[1];
				 String endPoint = args[2];

				 
				 JmsConnectionFactory jmsCF = JmsFactoryFactory.getInstance().createConnectionFactory();
				 JmsQueue jmsQ = JmsFactoryFactory.getInstance().createQueue(nomeCoda);
				 
				 jmsCF.setBusName(serverBus);
				 jmsCF.setProviderEndpoints(endPoint);
				 javax.jms.Connection con = jmsCF.createConnection();
				 
				 session = con.createSession(false, Session.AUTO_ACKNOWLEDGE);
				 
				 producer = session.createProducer(jmsQ);
				 
				 
				 Message mess = session.createObjectMessage(new String ("Hello"));
				 
				 
				 producer.send(mess);
				 con.close();
			 } 
			

		} catch (JMSException e) {

			e.printStackTrace();
			throw e;
		} // end catch
		catch (Exception e) {

			throw e;
		} // end catch

	}

	public static void main(String[] args) throws Exception {
		new TestJMS().testScriviCoda(args);
	}

}
